0) Сформировать токен
https://oauth.yandex.ru/authorize?response_type=token&display=popup&client_id=92ceae90b96d494e9f9bd21c99d43315

1) Очистить корзину
curl -s -H "Authorization: OAuth TOKEN" -X "DELETE" https://cloud-api.yandex.net/v1/disk/trash/resources/?path=

2) Информация о диске (если в конце добавить >> file_name.txt то ответ запишется в файл инкрементно, -o file_name.txt - ответ перезаписывается)
curl -s -H "Authorization: OAuth TOKEN" https://cloud-api.yandex.net/v1/disk/



Источники инфо:
https://yandex.ru/dev/disk/api/reference/upload-ext.html/
https://interface31.ru/tech_it/2019/09/ochistka-korziny-yandeksdiska-cherez-api-v-windows.html
https://interface31.ru/tech_it/2019/08/ochistka-korziny-yandeksdiska-cherez-api-v-debian-i-ubuntu.html
