# Service for automatization download set of pictures from internet and save it at YandexDisk
# For start: 
# $: python ydisk_pic.py
# enter urls of pictures via space
# see pictures in folder "PICTURES" at local machine and "PICTURES/" at YDISK
# for auth - in folder of this application must be file "yd-t.txt" with yandex auth token

import os
import subprocess
import json

print("enter urls of pictures:")
pic_urls = input()

tmp1 = pic_urls.split()
file_names = [t.split('/')[-1] for t in tmp1]

if not os.path.exists('PICTURES'):
    os.mkdir('PICTURES')

# authorize
with open('yd-t.txt', 'r') as f:
    t = f.read()
tkn = t.strip()
a = "Authorization: OAuth " + tkn


# 01 === download and save pictures to folder "./PICTURES"

r = subprocess.check_output(f'cd PICTURES && curl -s -O --remote-name-all {pic_urls} && cd ..', shell=True)

# 2 === upload file to ydisk
for f in file_names:
    print(f)
    
    # 2.1. = request url for upload
    h = 'https://cloud-api.yandex.net/v1/disk/resources/upload?path=PICTURES/' + f
    r = subprocess.check_output(f'curl -s -H "{a}" {h}', shell=True)
    r = json.loads(r.decode())
    print(r)
    url_for_upload = r['href']
    print(url_for_upload)

    # 2.2 = upload file to url
    subprocess.check_output(f'cd PICTURES && curl -T {f} --url {url_for_upload} && cd ..', shell=True)
